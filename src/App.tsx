import * as React from 'react';
import * as Bluebird from 'bluebird';
import './App.css';
import { RootState } from './redux/reducers';
import { connect } from 'react-redux';
import { actionCreators, CounterAction } from './redux/actions/counter';

import AsyncTracker from './AsyncTracker';
import { ThunkAction } from './redux/actions/interfaces';

interface AppProps { }

interface ConnectProps {
  counter: number;
  increment(c: number): CounterAction;
  delayIncrement(c: number): ThunkAction<Bluebird<void>>;
}

type Props = AppProps & ConnectProps;

const showPending = (): JSX.Element => {
    return (
        <span>Loading...</span>
    );
};

const showResolved = (): JSX.Element => {
    return (
        <span/>
    );
};

const sendToastNotification = () =>  {
    console.log('Delayed increment completed');
};

export const App: React.SFC<Props> = (props) => {
    const { increment, delayIncrement, counter} = props;
    return (
    <div className="App">
        <p>{props.counter}</p>
        <AsyncTracker
            id="delay-increment"
            pendingContent={showPending()}
            resolvedContent={showResolved()}
            onResolve={sendToastNotification}
        />
        <button id="increment-btn" onClick={() => increment(counter)}>Click to increment</button>
        <button id="delay-increment-btn" onClick={() => delayIncrement(counter)}>Click to increment slowly</button>
    </div>);
};

const mapStateToProps = (state: RootState, props: AppProps) => ({
  counter: state.counter.value
});

export default connect(mapStateToProps, {
    increment: actionCreators.increment,
    delayIncrement: actionCreators.delayIncrement
})(App);
